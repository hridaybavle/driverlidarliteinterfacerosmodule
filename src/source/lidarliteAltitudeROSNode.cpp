//////////////////////////////////////////////////////
//  altitudeNode.cpp
//
//  Created on: Jan 8, 2015
//      Author: Hriday Bavle
//
//  Last modification on: Jan 8, 2015
//      Author: Hriday Bavle
//
//////////////////////////////////////////////////////



//I/O stream
//std::cout
#include <iostream>
//ROS
#include "ros/ros.h"
//parrotARDrone
#include "lidarliteInterfaceROSModule.h"
//Comunications
#include "communication_definition.h"

using namespace std;

int main(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, MODULE_NAME_DRIVER_PELICAN_ALTITUDE);
    ros::NodeHandle n;

    cout<<"[ROSNODE] Starting droneAltitude"<<endl;

    //Vars
    AltitudeROSModule MyAltitudeROSModule;
    MyAltitudeROSModule.open(n,MODULE_NAME_DRIVER_PELICAN_ALTITUDE);

    try
    {
        //Read messages
        ros::spin();
        return 1;

    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }
}
