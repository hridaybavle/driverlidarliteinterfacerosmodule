//Drone
#include "lidarliteInterfaceROSModule.h"

using namespace std;


////// Altitude ////////
AltitudeROSModule::AltitudeROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

AltitudeROSModule::~AltitudeROSModule()
{

    return;
}

void AltitudeROSModule::init()
{
    std::cout << "AltitudeROSModule::init(), stackPath:" << stackPath << std::endl;
    try {
        XMLFileReader my_xml_reader(stackPath+"configs/drone"+cvg_int_to_string(idDrone)+"/px4flow_interface.xml");
        double tr_h, h_max;
        tr_h = my_xml_reader.readDoubleValue(  "px4flow_interface_config:tr_h" );
        h_max = my_xml_reader.readDoubleValue( "px4flow_interface_config:h_max" );
        h_lowpassfilter.setResponseTime(tr_h);
        h_lowpassfilter.enableSaturation( true, +0.0, +h_max);
	std::cout << "tr_h:"  << tr_h  << std::endl;
	std::cout << "h_max:" << h_max << std::endl;
    } catch ( cvg_XMLFileReader_exception &e) {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }
    h_lowpassfilter.reset();

    filtered_derivative_wcb.setTimeParameters( PX4FLOW_INT_FDWCB_PRE_TR,
                                               PX4FLOW_INT_FDWCB_POST_TR,
                                               PX4FLOW_INT_FDWCB_TDERIV,
                                               PX4FLOW_INT_FDWCB_TMEMORY,
                                               PX4FLOW_INT_FDWCB_SENSORFREQ);
    filtered_derivative_wcb.reset();
}

void AltitudeROSModule::close()
{

}

void AltitudeROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    AltitudePubl = n.advertise<droneMsgsROS::droneAltitude>(DRONE_DRIVER_SENSOR_ALTITUDE, 1, true);


    //Subscriber
    AltitudeSubs=n.subscribe("laser_altitude", 1, &AltitudeROSModule::altitudeCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool AltitudeROSModule::resetValues()
{
    return true;
}

//Start
bool AltitudeROSModule::startVal()
{
    return true;
}

//Stop
bool AltitudeROSModule::stopVal()
{
    return true;
}

//Run
bool AltitudeROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}


void AltitudeROSModule::altitudeCallback(const std_msgs::Float64::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    ros::Time current_timestamp = ros::Time::now();

    double zraw_t = (-1.0) * msg->data;
//  h_lowpassfilter.setInput( zraw_t );
//  z_t = h_lowpassfilter.getOutput();
    time_t tv_sec; suseconds_t tv_usec;
    {
    tv_sec  = current_timestamp.sec;
    tv_usec = current_timestamp.nsec / 1000.0;
    filtered_derivative_wcb.setInput( zraw_t, tv_sec, tv_usec);
    }

    double z_t, dz_t;
    filtered_derivative_wcb.getOutput( z_t, dz_t);


    //Read Altitude from navdata
     //AltitudeMsgs.header   = msg->header;
    // correct px4flow timestamp
    AltitudeMsgs.header.stamp  = current_timestamp;
    //Altitude needs to be put in [m], mavwork reference frame!!
    AltitudeMsgs.altitude = z_t;  // m
    AltitudeMsgs.var_altitude   = 0.0;
    // [m/s], mavwork reference frame
    AltitudeMsgs.altitude_speed = dz_t;
    AltitudeMsgs.var_altitude_speed = 0.0;

    publishAltitudeValue();
    return;
}


bool AltitudeROSModule::publishAltitudeValue()
{
    if(droneModuleOpened==false)
        return false;

    AltitudePubl.publish(AltitudeMsgs);
    return true;
}
